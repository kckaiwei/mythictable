﻿using System.Threading.Tasks;

namespace MythicTable.GameSession
{
    public interface ILiveClient
    {  
        Task ConfirmDelta(SessionDelta delta);

        //TODO #6: Change the SessionDelta to be the SessionOpDelta
        Task ConfirmOpDelta(SessionOpDelta delta);

        Task Undo();

        Task Redo();
    }
}
