﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MythicTable.GameSession;

namespace MythicTable
{
    public class LivePlayHub : Hub<ILiveClient>
    {
        public IEntityCollection EntityCollection { get; }
        public IGameState GameState { get; }

        public LivePlayHub(IEntityCollection entities, IGameState gamestate)
        {
            this.EntityCollection = entities;
            this.GameState = gamestate;
        }

        // TODO: #17: A difference between the GameState on the server and the GameState for player clients exists. This means a delta should not just be broadcast once applied
        // The GM client will resolve actions and submit 1 delta for the players and another delta for the server
        [HubMethodName("submitDelta")]
        public async Task<bool> RebroadcastDelta(SessionDelta delta)
        {
            // TODO #6: This should call the GameState ApplyDelta. EntityCollection functionality should be ported and generalised.
            var success = await this.EntityCollection.ApplyDelta(delta.Entities);

            if (success)
            {
                await this.Clients.All.ConfirmDelta(delta);
            }

            return success;
        }
        [HubMethodName("submitDeltaTemp")]
        public async Task<bool> RebroadcastDeltaTemp(SessionOpDelta delta)
        {
            // TODO #6: This should call the GameState ApplyDelta. EntityCollection functionality should be ported and generalised.
            var success = await this.GameState.ApplyDelta(delta);

            if (success)
            {
                await this.Clients.All.ConfirmOpDelta(delta);
            }

            return success;
        }
        [HubMethodName("submitUndo")]
        public async Task<bool> RebroadcastUndo(string undoPlaceHolder)
        {
            var success = await this.GameState.Undo(undoPlaceHolder);

            if (success)
            {
                await this.Clients.All.Undo();
            }

            return success;
        }

        [HubMethodName("submitRedo")]
        public async Task<bool> RebroadcastRedo(string redoPlaceholder)
        {
            var success = await this.GameState.Redo(redoPlaceholder);

            if (success)
            {
                await this.Clients.All.Redo();
            }

            return success;
        }
        
    }
}
